<?php

class PostMapper extends Mapper
{
    public function getPosts() {
        $sql = "SELECT * from post";
        $stmt = $this->db->query($sql);
        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new PostEntity($row);
        }
        
        return $results;
    }

    public function getFilteredPosts(Array $filterData) {
        $filter = $filterData['filter'];
        $search = $filterData['search'];
        $search = "%$search%";
        $sql = "SELECT * from post WHERE $filter LIKE :search";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':search', $search, PDO::PARAM_STR);
        $stmt->execute();

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new PostEntity($row);
        }
        
        return $results;
    }

    public function getExactFilteredPosts(Array $filterData) {
        $filter = $filterData['filter'];
        $search = $filterData['search'];
        $sql = "SELECT * from post WHERE $filter = :search";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':search', $search, PDO::PARAM_STR);
        $stmt->execute();

        $results = [];
        while($row = $stmt->fetch()) {
            $results[] = new PostEntity($row);
        }
        
        return $results;
    }



    public function savePost(PostEntity $post) {
        try{
            $sql = "INSERT INTO post (title, resName, author, postDate, content) 
                VALUES (:title, :resName, :author, :postDate, :content)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':title', $post->getTitle(), PDO::PARAM_STR);
            $stmt->bindParam(':resName', $post->getResName(), PDO::PARAM_STR);
            $stmt->bindParam(':author', $post->getAuthor(), PDO::PARAM_STR);
            $stmt->bindParam(':postDate', $post->getPostDate(), PDO::PARAM_STR);
            $stmt->bindParam(':content', $post->getContent(), PDO::PARAM_STR);

            $result = $stmt->execute();
        }catch(PDOException $e){
            echo 'Creacion fallida';
        }
        
        if(!$result) {
            throw new Exception("could not save record");
        }
    }

}