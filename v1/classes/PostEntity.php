<?php

class PostEntity
{
    public $postID;
    public $title;
    public $resName;
    public $author;
    public $postDate;
    public $content;

    public function __construct(array $data) {
        // no id if we're creating
        if(isset($data['postID'])) {
            $this->postID = $data['postID'];
        }
        $this->title = $data['title'];
        $this->resName = $data['resName'];
        $this->author = $data['author'];
        $this->postDate = $data['postDate'];
        $this->content = $data['content'];
    }

    public function getPostID() {
        return $this->postID;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getResName() {
        return $this->resName;
    }

    public function getAuthor() {
        return $this->author;
    }

    public function getPostDate() {
        return $this->postDate;
    }

    public function getContent() {
        return $this->content;
    }

}