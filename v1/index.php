<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\StreamInterface as Stream;
require_once 'vendor/autoload.php';

spl_autoload_register(function ($classname) {
    require ("classes/" . $classname . ".php");
});


$config['displayErrorDetails'] = true;

//Parametros de conexion a la BD
$config['db']['host']   = "localhost";
$config['db']['user']   = "root";
$config['db']['pass']   = "o1yxNoFJzK";
$config['db']['dbname'] = "blog";

$app = new \Slim\App(["settings" => $config]);
$container = $app->getContainer();

//Crear conexion a la BD
$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
    return $pdo;
};


$app->get('/posts', function (Request $request, Response $response) {
    //Obtener datos de la BD
    $mapper = new PostMapper($this->db);
    $posts = $mapper->getPosts();

    //Agregar datos a respuesta (PSR Response)
    $response = $response->withHeader('Content-Type','application/json');
    $response = $response->withStatus(200);    
    $body = $response->getBody();
    $body->write(json_encode(array('post'=>$posts)));
    $response = $response->withBody($body);
    return $response;
});

$app->get('/posts/{filter}/{search}', function (Request $request, Response $response, $args) {

    $check_array = array('title', 'author','content');
    if (in_array($args['filter'],$check_array)){
        $filterData = [];
        $filterData['filter'] = filter_var($args['filter'], FILTER_SANITIZE_STRING);
        $filterData['search'] = filter_var($args['search'], FILTER_SANITIZE_STRING);
        //Obtener datos de la BD
        $mapper = new PostMapper($this->db);
        $posts = $mapper->getFilteredPosts($filterData);

        //Agregar datos a respuesta (PSR Response)
        if(!empty($posts)){
            $response = $response->withHeader('Content-Type','application/json');
            $response = $response->withStatus(200);    
            $body = $response->getBody();
            $body->write(json_encode(array('post'=>$posts)));
            $response = $response->withBody($body);
        }else{
            $response = $response->withStatus(404);
        }
    }else{
        $response = $response->withStatus(400);
        $body = $response->getBody();
        $body->write('Search field not allowed. Use one of: title, author, content.');
        $response = $response->withBody($body);
    }

    
    
    return $response;
});

$app->post('/posts', function (Request $request, Response $response) {
    //Obtener datos del post a crear
    $data = $request->getParsedBody();

    $check_array = array('title', 'author', 'postDate','content');
    if (!array_diff($check_array, array_keys($data))){
        $postData = [];
        $postData['title'] = filter_var($data['title'], FILTER_SANITIZE_STRING);
        $postData['resName'] = str_replace(' ','_', $postData['title']);
        $postData['author'] = filter_var($data['author'], FILTER_SANITIZE_STRING);
        $postData['postDate'] = filter_var($data['postDate'], FILTER_SANITIZE_STRING);
        $postData['content'] = filter_var($data['content'], FILTER_SANITIZE_STRING);

        //Checar si ya existe resName
        $filterData = [];
        $filterData['filter'] = 'resName';
        $filterData['search'] = $postData['resName'];
        //Obtener datos de la BD
        $mapper = new PostMapper($this->db);
        $posts = $mapper->getExactFilteredPosts($filterData);

        if(empty($posts)){
            //Crear post
            $post = new PostEntity($postData);
            $postMapper = new PostMapper($this->db);
            $postMapper->savePost($post);

            //Crear respuesta
            //$response = $response->withHeader('Content-Type','application/json');
            $response = $response->withStatus(201);
            $body = $response->getBody();
            $body->write('Blog post created');
            $response = $response->withBody($body);
        }else{
            //$response = $response->withHeader('Content-Type','application/json');
            $response = $response->withStatus(409);
            $body = $response->getBody();
            $body->write('A blog post with that title already exists, please modify it and try again');
            $response = $response->withBody($body);
        }

        
    }else{
        //Crear respuesta
        //$response = $response->withHeader('Content-Type','application/json');
        $response = $response->withStatus(400);
        $body = $response->getBody();
        $body->write('One or more fields are incomplete, please complete them and try again');
        $response = $response->withBody($body);
    }

   
    return $response;
});

$app->run();